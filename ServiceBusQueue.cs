using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;

namespace StarFunction
{
    public static class ServiceBusQueue
    {
        [FunctionName("MessageIron")]
        public static string MessageFirst([ServiceBusTrigger("Iron", Connection = "AzureWebJobsStorage")]string item, ILogger log)
        {
            log.LogInformation($"The iron t-shirt successfully");
            return item;
        }

        [FunctionName("EndMessageAction")]
        public static void MessageSecond([ServiceBusTrigger("FryChicken", Connection = "AzureWebJobsStorage")]string item, ILogger log)
        {
            log.LogInformation($"The first queue done {item}");
            log.LogInformation($"The fry chicken and eat it");
        }
    }
}
