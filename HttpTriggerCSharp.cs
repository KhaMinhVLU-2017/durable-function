using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.WebJobs.Extensions.DurableTask;

namespace StarFunction
{
    public static class TriggerCSharp
    {
        public const string SPRAY = "spray";
        public const string CLEAR = "clear";
        public static int count = 0;

        [FunctionName("Triggerhome")]
        public static Task<string> HomeAction(
                            [HttpTrigger(AuthorizationLevel.Anonymous, "get")] HttpRequest req,
                            ILogger log)
        {

            string action = req.Query["action"]; //spray
            switch (action)
            {
                case SPRAY:
                    action = SPRAY;
                    log.LogInformation("The tree has been watered in log");
                    break;
                default:
                    action = null;
                    break;
            }
            var message = action != null
                     ? "The tree has been watered"
                     : "Please pass correct action";
            return Task.FromResult(message);
        }

        [FunctionName("ClearGarden")]
        public static Task<string> GardenAction(
                            [HttpTrigger(AuthorizationLevel.Anonymous, "get")] HttpRequest req,
                            string input,
                             ILogger log)
        {
            string action = req.Query["action"]; //clear
            switch (action)
            {
                case CLEAR:
                    action = CLEAR;
                    log.LogInformation("The garden is clear in log");
                    break;
                default:
                    action = null;
                    break;
            }
            var message = action != null
                ? "The garden is clear"
                : "Please pass correct action";
            return Task.FromResult(message);
        }

        [FunctionName("Orchestration")]
        public static async Task<List<string>> StartAll(
               [OrchestrationTrigger] IDurableOrchestrationContext context
        )
        {
            var outBehavior = new List<string>();
            outBehavior.Add(await context.CallActivityAsync<string>("Triggerhome", "spary"));
            outBehavior.Add(await context.CallActivityAsync<string>("ClearGarden", "clear"));

            return outBehavior;
        }

    }
}
